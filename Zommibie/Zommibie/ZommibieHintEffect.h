//
//  ZommibieHintSprite.h
//  Zommibie
//
//  Created by Static Ga on 12-12-2.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface ZommibieHintEffect : CCSprite {
    
}
+ (ZommibieHintEffect *)sprite;
- (void)startAnimation;
- (id)hintAction ;
@end
