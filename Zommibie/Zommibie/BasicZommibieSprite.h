//
//  BasicZommibieSprite.h
//  Zommibie
//
//  Created by Static Ga on 12-12-3.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
//标记僵尸的三种状态：正常、受伤、死亡
typedef enum {
    kZommibieSpriteNormal = 0,
    kZommibieSpriteInjured ,
    kZommibieSpriteDead,
}kZommibieSpriteState;
@interface BasicZommibieSprite : CCSprite {
    
}
@property (nonatomic) BOOL spriteCannotBeat;
- (void)runSpriteNormalAction;
- (void)spriteStateChanged:(kZommibieSpriteState)state;
@end
