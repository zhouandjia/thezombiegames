//
//  MikeZommibieInjuredSprite.m
//  Zommibie
//
//  Created by Static Ga on 12-12-2.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import "MikeZommibieInjuredSprite.h"

#define M_ZomInjured @"Mike.png"

@implementation MikeZommibieInjuredSprite
+ (MikeZommibieInjuredSprite *)sprite {
    return [[self alloc] initWithFile:M_ZomInjured];
}
@end
