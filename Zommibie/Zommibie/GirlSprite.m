//
//  GirlSprite.m
//  Zommibie
//
//  Created by Static Ga on 12-12-3.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import "GirlSprite.h"
#import "CCAnimation+Helper.h"

#define G_Normal @"Girl0000.png"
#define G_Prefix @"Girl000"
#define G_Injured @"Girl.png"
@implementation GirlSprite
+ (GirlSprite *)sprite {
    return [[self alloc] initWithFile:G_Normal];
}

- (void)runSpriteNormalAction {
    id action = [self getNormalAction];
    [self runAction:[CCRepeatForever actionWithAction:action]];
}

- (id)getNormalAction {
    CCAnimation *animation = [CCAnimation animationWithFile:G_Prefix frameCount:14 delay:0.2];
    id action = [CCAnimate actionWithAnimation:animation];
    return action;
}

- (void)runSpriteInjuredAction {
    //    [self stopAllActions];
    
    CCTexture2D * texture =[[CCTextureCache sharedTextureCache] addImage: G_Injured];//新建贴图
    [self setTexture:texture];
}

- (void)stateChanged {
    CCTexture2D * texture =[[CCTextureCache sharedTextureCache] addImage: @"Mike.png"];//新建贴图
    [self setTexture:texture];
}

- (void)spriteStateChanged:(kZommibieSpriteState)state {
    switch (state) {
        case kZommibieSpriteNormal:
        {
            [self runSpriteNormalAction];
        }
            break;
        case kZommibieSpriteInjured:
        {
            [self runSpriteInjuredAction];
        }
            break;
        case kZommibieSpriteDead:
        {
            
        }
            break;
        default:
            break;
    }
}

@end
