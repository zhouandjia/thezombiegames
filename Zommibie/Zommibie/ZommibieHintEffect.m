//
//  ZommibieHintSprite.m
//  Zommibie
//
//  Created by Static Ga on 12-12-2.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import "ZommibieHintEffect.h"
#import "CCAnimation+Helper.h"
#define Z_HintNormal @"hit0000.png"
#define Z_HintFileName @"hit000"
@implementation ZommibieHintEffect
+ (ZommibieHintEffect *)sprite {

    return [self spriteWithFile:Z_HintNormal];
}

- (void)startAnimation {
   CCAnimation *animation = [CCAnimation animationWithFile:Z_HintFileName frameCount:3 delay:0.1];
    CCAnimate *animate = [CCAnimate actionWithAnimation:animation];
    CCRepeatForever *rep = [CCRepeatForever actionWithAction:animate];
    [self runAction:rep];
}
- (id)hintAction {
    CCAnimation *animation = [CCAnimation animationWithFile:Z_HintFileName frameCount:3 delay:0.1];
    CCAnimate *animate = [CCAnimate actionWithAnimation:animation];
//    CCRepeatForever *rep = [CCRepeatForever actionWithAction:animate];
    return animate;
}
@end
