//
//  Constant.h
//  Zommibie
//
//  Created by Static Ga on 12-12-1.
//
//

#ifndef Zommibie_Constant_h
#define Zommibie_Constant_h

#define SafeRelease(_point) {if (_point) {[_point release];_point = nil;}}
typedef void (^ZZBlockVoid)(void);
#endif
