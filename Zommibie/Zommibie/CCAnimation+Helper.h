//
//  CCAnimation+Helper.h
//  Zommibie
//
//  Created by Static Ga on 12-12-2.
//
//

#import "CCAnimation.h"

@interface CCAnimation (Helper)
+(CCAnimation*) animationWithFile:(NSString*)name frameCount:(int)frameCount delay:(float)delay;
+(CCAnimation*) animationWithFrame:(NSString*)frame frameCount:(int)frameCount delay:(float)delay;
@end
