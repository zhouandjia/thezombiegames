//
//  ZommibieMenu.h
//  Zommibie
//
//  Created by Static Ga on 12-12-1.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface ZommibieMenu : CCLayer {
    NSInteger currentRandom;
}
//标记九个点
@property (nonatomic, retain) NSMutableArray *pointArray;
//标记九个僵尸
@property (nonatomic, retain) NSMutableArray *zommibiesArray;
@property (nonatomic, retain) NSMutableArray *activeZommibiesArray;
+ (CCScene *)scene;
@end
