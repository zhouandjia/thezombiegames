//
//  MikeZommibie.m
//  Zommibie
//
//  Created by Static Ga on 12-12-1.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import "MikeZommibie.h"
#import "Constant.h"

#define M_ZomNormal @"Mike0001.png"
#define M_ZomInjured @"Mike.png"

#define M_ZomArm  @"arm.png"
#define M_ZomBody  @"body.png"
#define M_ZomHead  @"head.png"
#define M_ZomLeg  @"leg.png"
#define M_ZomHand  @"hand.png"

@implementation MikeZommibie

- (id)init {
    if (self = [super init]) {
        
    }
    return self;
}
+ (MikeZommibie *)sprite {
    return [[[self alloc] initWithFile:M_ZomNormal] autorelease];
}

- (void)runSpriteNormalAction {
    
    
    NSMutableArray *frames = [[NSMutableArray alloc] init];
    NSString *firstFrameName = nil;
    for (int i = 1; i < 10; i++) {
        firstFrameName = nil;
        firstFrameName = [NSString stringWithFormat:@"Mike000%d.png", i];

        CCTexture2D* texture = [[CCTextureCache sharedTextureCache] addImage:firstFrameName];
        CGSize texSize = texture.contentSize;
        CGRect texRect = CGRectMake(0, 0, texSize.width, texSize.height);
        CCSpriteFrame* frame = [CCSpriteFrame frameWithTexture:texture rect:texRect];
        
        [frames addObject:frame];
    }
    for (int j = 10; j < 14; j++) {
        firstFrameName = nil;
        firstFrameName = [NSString stringWithFormat:@"Mike00%d.png", j];

        CCTexture2D* texture = [[CCTextureCache sharedTextureCache] addImage:firstFrameName];
        CGSize texSize = texture.contentSize;
        CGRect texRect = CGRectMake(0, 0, texSize.width, texSize.height);
        CCSpriteFrame* frame = [CCSpriteFrame frameWithTexture:texture rect:texRect];
        
        [frames addObject:frame];

    }
    CCAnimation *animation = [CCAnimation animationWithSpriteFrames:frames delay:0.1];
    id action = [CCAnimate actionWithAnimation:animation];
    
    [self runAction:[CCRepeatForever actionWithAction:action]];
}

- (void)runSpriteInjuredAction {
//    [self stopAllActions];

//    CCTexture2D * texture =[[CCTextureCache sharedTextureCache] addImage: M_ZomInjured];//新建贴图
//    [self setTexture:texture];
    
    [self runSpriteDeadAction];
}

- (void)runSpriteDeadAction {
    CGSize zomSize = self.contentSize;
    /*
    CCSprite *leftarmSprite = [CCSprite spriteWithFile:M_ZomArm];
    CCSprite *leftlegSprite = [CCSprite spriteWithFile:M_ZomLeg];
    leftlegSprite.position = ccp(zomSize.width/2 - leftlegSprite.contentSize.width/2, leftlegSprite.contentSize.height/2);
 
    CCSprite *bodySprite = [CCSprite spriteWithFile:M_ZomBody];
    bodySprite.position = ccp(zomSize.width/2, leftlegSprite.contentSize.height + bodySprite.contentSize.height/2);
   
    CCSprite *headSprite = [CCSprite spriteWithFile:M_ZomHead];
    headSprite.position = ccp(zomSize.width/2, zomSize.height - headSprite.contentSize.height/2);
    

    CCSprite *lefthandSprite = [CCSprite spriteWithFile:M_ZomHand];
    lefthandSprite.position = ccp(lefthandSprite.contentSize.width/2, bodySprite.position.y + bodySprite.contentSize.height/2 - lefthandSprite.contentSize.height/2);

    CCSprite *rightarmSprite = [CCSprite spriteWithFile:M_ZomArm];
    CCSprite *rightlegSprite = [CCSprite spriteWithFile:M_ZomLeg];
    rightlegSprite.position = ccp(zomSize.width/2 + rightlegSprite.contentSize.width/2,rightlegSprite.contentSize.height/2);
    CCSprite *righthandSprite = [CCSprite spriteWithFile:M_ZomHand];
    righthandSprite.position = ccp(zomSize.width - righthandSprite.contentSize.width/2, bodySprite.position.y + bodySprite.contentSize.height/2 - righthandSprite.contentSize.height/2);
    
    headSprite.position = [headSprite.parent convertToWorldSpace:headSprite.position];
    [self.parent addChild:headSprite];
    [self addChild:bodySprite];
    
//    [self addChild:leftarmSprite];
    [self addChild:leftlegSprite];
    [self addChild:lefthandSprite];
    
//    [self addChild:rightarmSprite];
    [self addChild:rightlegSprite];
    [self addChild:righthandSprite];
    
    CCLOG(@"headcover %@",NSStringFromCGPoint([headSprite convertToWorldSpace:headSprite.position]));
*/
}

- (id)getInjuredAction {
    NSMutableArray *frames = [NSMutableArray arrayWithCapacity:1];
    CCTexture2D* texture = [[CCTextureCache sharedTextureCache] addImage:M_ZomInjured];
    CGSize texSize = texture.contentSize;
    CGRect texRect = CGRectMake(0, 0, texSize.width, texSize.height);
    CCSpriteFrame* frame = [CCSpriteFrame frameWithTexture:texture rect:texRect];
    [frames addObject:frame];
    
    CCAnimation *animation = [CCAnimation animationWithSpriteFrames:frames delay:0.1];
    id action = [CCAnimate actionWithAnimation:animation];
    return action;
}

- (void)spriteStateChanged:(kZommibieSpriteState)state{
    switch (state) {
        case kZommibieSpriteNormal:
        {
            [self runSpriteNormalAction];
        }
            break;
        case kZommibieSpriteInjured:
        {
            [self runSpriteInjuredAction];
        }
            break;
        case kZommibieSpriteDead:
        {
            
        }
            break;
        default:
            break;
    }
}
@end
