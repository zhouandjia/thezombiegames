//
//  MikeZommibie.h
//  Zommibie
//
//  Created by Static Ga on 12-12-1.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "BasicZommibieSprite.h"

@interface MikeZommibie : BasicZommibieSprite {
    
}
- (void)runSpriteNormalAction;
+ (MikeZommibie *)sprite;
- (void)runSpriteInjuredAction ;
- (id)getInjuredAction ;
@end
