//
//  ZommibieMenu.m
//  Zommibie
//
//  Created by Static Ga on 12-12-1.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import "ZommibieMenu.h"
#import "Constant.h"
#import "MikeZommibie.h"
#import "ZommibieHintEffect.h"
#import "MikeZommibieInjuredSprite.h"
#import "GirlSprite.h"
#import "BasicZommibieSprite.h"

//M:表示在menu中，bg：背景的缩写
#define M_BgLayer1 @"layer1.png"
#define M_BgLayer2 @"layer2.png"
#define M_BgLayer3 @"layer3.png"
#define M_BgLayer4 @"layer4.png"

#define M_ZomArm  @"arm.png"
#define M_ZomBody  @"body.png"
#define M_ZomHead  @"head.png"
#define M_ZomLeg  @"leg.png"
#define M_ZomHand  @"hand.png"


@implementation ZommibieMenu
+ (CCScene *)scene {
    CCScene *scene = [CCScene node];
    ZommibieMenu *layer = [ZommibieMenu node];
    [scene addChild:layer];
    return scene;
}

- (void)dealloc {
    SafeRelease(_pointArray);
    SafeRelease(_zommibiesArray);
    SafeRelease(_activeZommibiesArray);
    [super dealloc];
}
- (id)init {
    if (self = [super init]) {
        self.isTouchEnabled = YES;

        [self addBgUI];
        self.pointArray = [[[NSMutableArray alloc] init] autorelease];
        self.zommibiesArray =[[[NSMutableArray alloc] init] autorelease];
        self.activeZommibiesArray = [[[NSMutableArray alloc] init] autorelease];
        [self addPointToArray];
        [self addZommibies];
        
        
        [self randomZommibies];
    }
    return self;
}

- (void)addBgUI {
    
    CCSprite *sprite1 = [CCSprite spriteWithFile:M_BgLayer1];
    sprite1.position = CGPointMake(sprite1.contentSize.width/2, sprite1.contentSize.height/2);
    [self addChild:sprite1 z:4];
    
    CCSprite *sprite2 = [CCSprite spriteWithFile:M_BgLayer2];
    sprite2.position = CGPointMake(sprite2.contentSize.width/2, sprite1.contentSize.height + sprite2.contentSize.height/2);
    [self addChild:sprite2 z:3];

    CCSprite *sprite3 = [CCSprite spriteWithFile:M_BgLayer3];
    sprite3.position = CGPointMake(sprite3.contentSize.width/2, sprite2.position.y + sprite2.contentSize.height/2 + sprite3.contentSize.height/2);
    [self addChild:sprite3 z:2];

    CCSprite *sprite4 = [CCSprite spriteWithFile:M_BgLayer4];
    sprite4.position = CGPointMake(sprite4.contentSize.width/2, sprite3.position.y + sprite3.contentSize.height/2 +sprite4.contentSize.height/2);
    [self addChild:sprite4 z:1];

}

- (void)addPointToArray {
    for (int i = 0; i < 9; i++) {
        CGPoint point;
        //行
        NSInteger row = i%3;
        NSLog(@"row is %i",row);
        //列
        NSInteger column = i/3;
        point.y = [self makeCenterPointX:column];
        point.x = [self makeCenterPointY:row];
        [self.pointArray addObject:[NSValue valueWithCGPoint:point]];
    }
}

//X
- (CGFloat)makeCenterPointX :(NSInteger)column{
    CGFloat centerX = 0.0;
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    switch (column) {
        case 0:
        {
            centerX = winSize.width/4 - 20;
        }
            break;
        case 1:
        {
            centerX = winSize.width/2;
        }
            break;
        case 2:
        {
            centerX = winSize.width - winSize.width/4 + 20;
        }
            break;
        default:
            break;
    }
    return centerX;
}

//Y
- (CGFloat)makeCenterPointY :(NSInteger)row {
    CGFloat centerY = 0.0;
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    switch (row) {
        case 0:
        {
            centerY = winSize.height/6 + 60;
        }
            break;
        case 1:
        {
            centerY = winSize.height/2 + 5;
        }
            break;
        case 2:
        {
            centerY = winSize.height - winSize.height/6 - 50;
        }
            break;
        default:
            break;
    }
    return centerY;
}

- (void)addZommibies {
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    for (int i = 0; i < 9; i++) {
        MikeZommibie *zommi = [MikeZommibie sprite];
        zommi.position = CGPointMake(-winSize.width/2, -winSize.height/2);
        [self addChild:zommi];
        [self.zommibiesArray addObject:zommi];
    }
    for (int i = 0; i < 9; i++) {
        GirlSprite *zommi = [GirlSprite sprite];
        zommi.position = CGPointMake(-winSize.width/2, -winSize.height/2);
        [self addChild:zommi];
        [self.zommibiesArray addObject:zommi];

    }
    NSLog(@"self.zomibiesarray count %i",self.zommibiesArray.count);
}

//随机出现
- (void)randomZommibies {
    int x = arc4random() % 9;
    
    NSLog(@"random num %i",x);
    
    int row = x/3;
    NSInteger z = 0;
    switch (row) {
        case 0:
        {
            z = 3;
        }
            break;
        case 1:
        {
            z = 2;
        }
            break;
        case 2:
        {
            z = 1;
        }
            break;
        default:
        {
            z = 4;
        }
            break;
    }
    int randomZommibie = arc4random() % 18;
    currentRandom = randomZommibie;

    BasicZommibieSprite *zommibie = [self.zommibiesArray objectAtIndex:randomZommibie];
    [zommibie stopAllActions];
    zommibie.spriteCannotBeat = YES;
    CGPoint randomPoint = [[self.pointArray objectAtIndex:x] CGPointValue];
    
    zommibie.position = CGPointMake(randomPoint.x, randomPoint.y - zommibie.contentSize.height);
    [self reorderChild:zommibie z:z];//改变sprite的层次

    CCMoveBy *move = [CCMoveBy actionWithDuration:0.5 position:CGPointMake(0, zommibie.contentSize.height)];
    [zommibie runAction:move];    
    [zommibie runSpriteNormalAction];

    //1秒钟后消失
    CCDelayTime *delay = [CCDelayTime actionWithDuration:1.0];
    CCCallFuncO *f = [CCCallFuncO actionWithTarget:self selector:@selector(zoomibiesDisappear:) object:[NSNumber numberWithInt:randomZommibie]];
    CCSequence *que = [CCSequence actions:delay, f,nil];
    [zommibie runAction:que];
}
//移除僵尸
- (void)zoomibiesDisappear :(NSNumber *)index{
    
    int value = index.intValue;
    
    BasicZommibieSprite *sprite = [self.zommibiesArray objectAtIndex:value];
    sprite.spriteCannotBeat = NO;
    CCMoveBy *move = [CCMoveBy actionWithDuration:0.5 position:CGPointMake(0, -sprite.contentSize.height)];
    CCCallFunc *fun = [CCCallFunc actionWithTarget:self selector:@selector(randomZommibies)];
    [sprite runAction:[CCSequence actions:move,fun, nil]];
}

- (CGRect )returnSpriteRect:(CCSprite *)sprite
{
    CGPoint position = sprite.position;
    CGSize contentSize = sprite.contentSize;
    return CGRectMake(position.x - contentSize.width/2, position.y - contentSize.height/2, contentSize.width, contentSize.height);
}

//touch
- (void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    NSArray *touchesArray = [touches allObjects];
    for (UITouch *touch in touchesArray) {
        CGPoint location = [touch locationInView:[touch view]];
        location = [[CCDirector sharedDirector] convertToGL:location];
        for (BasicZommibieSprite *zombie in self.zommibiesArray) {
            
            if (zombie.numberOfRunningActions && zombie.spriteCannotBeat) {
                if (CGRectContainsPoint([self returnSpriteRect:zombie], location) ){
                    [self addHitOnSprite:zombie withPosition:location];
                    break;
                }
            }
        }
    }
}
//添加打击效果
- (void)addHitOnSprite:(CCSprite *)sprite withPosition:(CGPoint)point{
    
    BasicZommibieSprite *basicZommibie = (BasicZommibieSprite *)sprite;
    [basicZommibie stopAllActions];
    basicZommibie.spriteCannotBeat = NO;

    ZommibieHintEffect *hitSprite = [ZommibieHintEffect sprite];
    hitSprite.position = point;
    [self addChild:hitSprite z:5];
    
    [hitSprite runAction:[CCSequence actions:[hitSprite hintAction], [CCCallBlock actionWithBlock:^(void){
        [hitSprite stopAllActions];
        [hitSprite removeFromParentAndCleanup:YES];
    }],
                          nil]];

    [basicZommibie spriteStateChanged:kZommibieSpriteInjured];
    
    if ([basicZommibie isKindOfClass:[MikeZommibie class]]) {
        CGSize zomSize = basicZommibie.contentSize;
        CGPoint zomPoint = basicZommibie.position;
        CCSprite *leftlegSprite = [CCSprite spriteWithFile:M_ZomLeg];
        
        leftlegSprite.position = ccp(zomPoint.x - leftlegSprite.contentSize.width/2, zomPoint.y - zomSize.height/2 + leftlegSprite.contentSize.height/2);
        
        CCSprite *bodySprite = [CCSprite spriteWithFile:M_ZomBody];
        bodySprite.position = ccp(zomPoint.x, leftlegSprite.contentSize.height/2 + leftlegSprite.position.y + bodySprite.contentSize.height/2);
        
        CCSprite *headSprite = [CCSprite spriteWithFile:M_ZomHead];
        headSprite.position = ccp(zomPoint.x, bodySprite.position.y + bodySprite.contentSize.height/2 + headSprite.contentSize.height/3);
        
        CCSprite *lefthandSprite = [CCSprite spriteWithFile:M_ZomHand];
        lefthandSprite.position = ccp(zomPoint.x - lefthandSprite.contentSize.width/2, bodySprite.position.y + bodySprite.contentSize.height/2 - lefthandSprite.contentSize.height/2);
        CCSprite *leftarmSprite = [CCSprite spriteWithFile:M_ZomArm];
        leftarmSprite.position = ccp(lefthandSprite.position.x, lefthandSprite.position.y);
        
        CCSprite *rightlegSprite = [CCSprite spriteWithFile:M_ZomLeg];
        rightlegSprite.position = ccp(zomPoint.x + rightlegSprite.contentSize.width/2, leftlegSprite.position.y);
        CCSprite *righthandSprite = [CCSprite spriteWithFile:M_ZomHand];
        CCSprite *rightarmSprite = [CCSprite spriteWithFile:M_ZomArm];
        rightarmSprite.position = ccp(righthandSprite.position.x, righthandSprite.position.y);
        righthandSprite.position = ccp(zomPoint.x + righthandSprite.contentSize.width/2, lefthandSprite.position.y);
        
        
        [self addChild:bodySprite z:basicZommibie.zOrder tag:0];
        
        [self addChild:leftarmSprite z:basicZommibie.zOrder tag:0];
        [self addChild:leftlegSprite z:basicZommibie.zOrder tag:0];
        
        [self addChild:rightarmSprite z:basicZommibie.zOrder tag:0];
        [self addChild:rightlegSprite z:basicZommibie.zOrder tag:0];
        
        [self addChild:headSprite z:basicZommibie.zOrder tag:0];
        
        [self addChild:righthandSprite z:basicZommibie.zOrder tag:0];
        [self addChild:lefthandSprite z:basicZommibie.zOrder tag:0];
        
        NSMutableArray *partArray =[NSMutableArray arrayWithObjects:headSprite,bodySprite, leftarmSprite, lefthandSprite, rightarmSprite, righthandSprite,leftlegSprite, rightlegSprite,nil];
        basicZommibie.position = ccp(0, -sprite.contentSize.height);
        
        CGSize winSize = [[CCDirector sharedDirector] winSize];
        for (CCSprite *sprite in partArray) {
            int value = arc4random() % (int)winSize.width;
            NSLog(@"value is %i",value);
            [self moveWithParabola:sprite startP:sprite.position endP:ccp(value, 0) startA:30 endA:80 dirTime:0.8];
            
        }
    }else if ([basicZommibie isKindOfClass:[GirlSprite class]]) {
        //do someting for girl
        basicZommibie.position = ccp(0, -sprite.contentSize.height);
    }
    

    [self randomZommibies];
//    CCCallFunc *fun = [CCCallFunc actionWithTarget:self selector:@selector(randomZommibies)];
//
//    CCMoveBy *move = [CCMoveBy actionWithDuration:1.0 position:CGPointMake(0, -sprite.contentSize.height)];
//    [basicZommibie runAction:[CCSequence actions:move,fun,nil]];
}

//  抛物线运动并同时旋转   
//mSprite：需要做抛物线的精灵
//startPoint:起始位置
//endPoint:中止位置
//startA:起始角度
//endA:中止角度
//dirTime:起始位置到中止位置的所需时间
- (void) moveWithParabola:(CCSprite*)mSprite startP:(CGPoint)startPoint endP:(CGPoint)endPoint startA:(float)startAngle endA:(float)endAngle dirTime:(float)time{
    float sx = startPoint.x;
    float sy = startPoint.y;
    float ex =endPoint.x+50;
    float ey =endPoint.y+150;
    int h = [mSprite contentSize].height*0.5;
    //设置精灵的起始角度
    mSprite.rotation=startAngle;
    ccBezierConfig bezier; // 创建贝塞尔曲线
    bezier.controlPoint_1 = ccp(sx, sy); // 起始点
    bezier.controlPoint_2 = ccp(sx+(ex-sx)*0.5, sy+(ey-sy)*0.5+200); //控制点
    bezier.endPosition = ccp(endPoint.x-30, endPoint.y+h); // 结束位置
    CCBezierTo *actionMove = [CCBezierTo actionWithDuration:time bezier:bezier];
    //创建精灵旋转的动作
    CCRotateTo *actionRotate =[CCRotateTo actionWithDuration:time angle:endAngle];
    //将两个动作封装成一个同时播放进行的动作

    CCAction * action = [CCSpawn actions:actionMove, actionRotate,nil];
    [mSprite runAction:action];
    [mSprite runAction:[CCSequence actions:[CCDelayTime actionWithDuration:time],[CCCallBlock actionWithBlock:^{
        [mSprite removeFromParentAndCleanup:YES];
    }],nil]];
}
@end
