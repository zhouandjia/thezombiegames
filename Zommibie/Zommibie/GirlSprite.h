//
//  GirlSprite.h
//  Zommibie
//
//  Created by Static Ga on 12-12-3.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "BasicZommibieSprite.h"

@interface GirlSprite : BasicZommibieSprite {
    
}
+ (GirlSprite *)sprite;
- (void)runSpriteNormalAction;
- (void)runSpriteInjuredAction ;
- (id)getNormalAction;
@end
