//
//  MyCocos2DClass.m
//  Zommibie
//
//  Created by Static Ga on 12-12-5.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import "ZommibieDeadParticle.h"


@implementation ZommibieDeadParticle
+(id)scene
{
    CCScene* scene=[CCScene node];
    ZommibieDeadParticle* zommibieDead=[ZommibieDeadParticle node];
    [scene addChild:zommibieDead];
    return scene;
}

-(id) init
{
	return [self initWithTotalParticles:40];
}

-(id) initWithTotalParticles:(NSUInteger)p
{
	if( (self=[super initWithTotalParticles:p]) ) {
        
		// duration
		duration = 0.1f;
		
		self.emitterMode = kCCParticleModeGravity;
        
		// Gravity Mode: gravity
		self.gravity = ccp(0,0);
		
		// Gravity Mode: speed of particles
		self.speed = 300;
		self.speedVar =100;
		
		// Gravity Mode: radial
		self.radialAccel = 0;
		self.radialAccelVar = 0;
		
		// Gravity Mode: tagential
		self.tangentialAccel = 0;
		self.tangentialAccelVar = 0;
		
		// angle
		angle = 90;
		angleVar = 360;
        
		// emitter position
		CGSize winSize = [[CCDirector sharedDirector] winSize];
		self.position = ccp(winSize.width/2, winSize.height/2);
		posVar = CGPointZero;
		
		// life of particles
		life = 5;
		lifeVar = 0;
		
		// size, in pixels
		startSize = 15.0f;
		startSizeVar = 10.0f;
		endSize = kCCParticleStartSizeEqualToEndSize;
        
		// emits per second
		emissionRate = totalParticles/duration;
		
		// color of particles
		startColor.r = 0.5f;
		startColor.g = 0.5f;
		startColor.b = 0.5f;
		startColor.a = 1.0f;
		startColorVar.r = 0.5f;
		startColorVar.g = 0.5f;
		startColorVar.b = 0.5f;
		startColorVar.a = 0.0f;
		endColor.r = 0.0f;
		endColor.g = 0.0f;
		endColor.b = 0.0f;
		endColor.a = 0.0f;
		endColorVar.r = 0.0f;
		endColorVar.g = 0.0f;
		endColorVar.b = 0.0f;
		endColorVar.a = 0.0f;
		
		self.texture = [[CCTextureCache sharedTextureCache] addImage: @"白星副本.png"];
        
		// additive
		//self.blendAdditive = NO;
	}
	
	return self;
}

- (void)dealloc
{
    [super dealloc];
}


@end
